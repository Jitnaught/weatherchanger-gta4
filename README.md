Weather Changer is a simple script that changes the in-game weather. Pretty straight forward, right?

How to install:
First install .NET Scripthook, then copy WeatherChanger.net.dll and WeatherChanger.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + W = Toggle the menu
NumPad2 = Scroll down
NumPad8 = Scroll up
NumPad5 = Set weather
-Keys are changeable in the ini file

Credits:
jpm1: for requesting the script
LetsPlayOrDy (now named Jitnaught): for writing the script

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
