﻿using GTA;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace WeatherChanger
{
    public class WeatherChanger : Script
    {
        bool menuOpen = false, useHoldKey;
        Keys pressKey, holdKey, nextKey, previousKey, selectKey;
        Color notSelectedColor, selectedColor;
        uint selectedItem = 0;
        Array weathers;

        public WeatherChanger()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("toggle_menu_press_key", "keys", Keys.W);
                Settings.SetValue("toggle_menu_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("use_hold_key", "keys", true);
                Settings.SetValue("next_weather_key", "keys", Keys.NumPad2);
                Settings.SetValue("previous_weather_key", "keys", Keys.NumPad8);
                Settings.SetValue("set_weather_key", "keys", Keys.NumPad5);
                Settings.SetValue("not_selected_color", "colors", Color.White.Name);
                Settings.SetValue("selected_color", "colors", Color.Blue.Name);
                Settings.Save();
            }

            pressKey = Settings.GetValueKey("toggle_menu_press_key", "keys", Keys.W);
            holdKey = Settings.GetValueKey("toggle_menu_hold_key", "keys", Keys.RControlKey);
            useHoldKey = Settings.GetValueBool("use_hold_key", "keys", true);
            nextKey = Settings.GetValueKey("next_weather_key", "keys", Keys.NumPad2);
            previousKey = Settings.GetValueKey("previous_weather_key", "keys", Keys.NumPad8);
            selectKey = Settings.GetValueKey("set_weather_key", "keys", Keys.NumPad5);
            try
            {
                notSelectedColor = Color.FromName(Settings.GetValueString("not_selected_color", "colors", Color.White.Name));
                selectedColor = Color.FromName(Settings.GetValueString("selected_color", "colors", Color.Blue.Name));
            }
            catch (Exception)
            {
                notSelectedColor = Color.White;
                selectedColor = Color.Blue;
                string tempString = "Weather Changer: You did not enter a correct color for either not_selected_color, or selected_color, or both.";
                Game.Console.Print(tempString);
                Game.DisplayText(tempString);
            }

            weathers = Enum.GetValues(typeof(Weather));

            KeyDown += WeatherChanger_KeyDown;
            PerFrameDrawing += WeatherChanger_PerFrameDrawing;
        }

        private void WeatherChanger_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey) Subtitle("Weather Changer menu is now " + ((menuOpen = !menuOpen) ? "open" : "closed"));

            if (menuOpen)
            {
                if (e.Key == nextKey && selectedItem < weathers.Length - 1)
                    selectedItem++;
                else if (e.Key == previousKey && selectedItem > 0)
                    selectedItem--;
                else if (e.Key == selectKey)
                {
                    ForceWeatherNow((Weather)weathers.GetValue(selectedItem));
                    Subtitle("Set weather to: " + GetWeatherName((Weather)weathers.GetValue(selectedItem)));
                }
            }
        }

        private void WeatherChanger_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (menuOpen)
            {
                uint y = 20;

                for (uint u = 0; u < weathers.Length; u++)
                {
                    e.Graphics.DrawText(GetWeatherName((Weather)weathers.GetValue(u)), 20, y, ((u == selectedItem) ? selectedColor : notSelectedColor));
                    y += 30;
                }
            }
        }


        private string GetWeatherName(Weather weather)
        {
            switch ((int)weather)
            {
                case 9: return "SunnyAndWindy2";
                case 8: return "ExtraSunny2";
                case 7: return "ThunderStorm";
                case 6: return "Foggy";
                case 5: return "Drizzle";
                case 4: return "Raining";
                case 3: return "Cloudy";
                case 2: return "SunnyAndWindy";
                case 1: return "Sunny";
                case 0: return "ExtraSunny";
                default: return "error";
            }
        }

        private void ForceWeatherNow(Weather weather)
        {
            GTA.Native.Function.Call("FORCE_WEATHER_NOW", (int)weather);
        }

        private void Subtitle(string text, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }
    }
}
